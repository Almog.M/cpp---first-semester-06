#include "Pentagon.h"
#include "ShapeException.h"




Pentagon::Pentagon(string name, string color, double rib) : Shape(name, color)
{
	setRib(rib);
}

void Pentagon::draw()
{
	std::cout << std::endl << "Color is " << getColor() << std::endl << "Name is " << getName() << std::endl << "area is " << CalArea() << std::endl << std::endl;

}


double Pentagon::CalArea()
{
	return MathUtils::CalPentagonArea(_rib);
}

Pentagon::~Pentagon()
{
}

void Pentagon::setRib(double rib)
{
	_rib = rib;
	if (_rib < 0)
		throw shapeException();
}

double Pentagon::getRib() const
{
	return _rib;
}
