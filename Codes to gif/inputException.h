#pragma once
#include <iostream>
#include <exception>
#include <string>
using std::string;
#define ENTER_CHAR '\n'
class buffer
{
public:
	void clearBuffer();
};


class inputException : public std::exception 
{
private:
	const char* _exception;
public:
	inputException();
	inputException(const char* exception);
	virtual ~inputException();
	virtual const char* what() const;
	void checkInputValidation(const char* possibleException) const; //If the input for any variable is not valid - throw an exception.
};
