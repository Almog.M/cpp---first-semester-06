#include <iostream>
#include <Math.h>
#include <string>
#define BAD_CALCULATION 0 //If the calculation or the arguments\local variables are 8200.
#define GOOD_CALCULATION 1 //Else.
#define FORBIDDEN_NUMBER 8200
#define DENIDE_MESSAGE "This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year."

typedef int(*fPtr) (int, int, bool*);

int add(int a, int b, bool *pValidCalculation) {
	if (a == FORBIDDEN_NUMBER || b == FORBIDDEN_NUMBER || a + b == FORBIDDEN_NUMBER)
	{
		*pValidCalculation = BAD_CALCULATION;
	}
	else
	{
		*pValidCalculation = GOOD_CALCULATION;
	}
	return a + b;
}

int  multiply(int a, int b, bool *pValidCalculation) {
	int sum = 0;
	for (int i = 0; i < b; i++) {
		sum = add(sum, a, pValidCalculation);
	};
	if (a == FORBIDDEN_NUMBER || b == FORBIDDEN_NUMBER || sum == FORBIDDEN_NUMBER)
	{
		*pValidCalculation = BAD_CALCULATION;
	}
	else
	{
		*pValidCalculation = GOOD_CALCULATION;
	}
	return sum;
}

int  pow(int a, int b, bool *pValidCalculation) {
	int exponent = 1;
	for (int i = 0; i < b; i++) {
		exponent = multiply(exponent, a, pValidCalculation);
	};
	if (a == FORBIDDEN_NUMBER || b == FORBIDDEN_NUMBER || exponent == FORBIDDEN_NUMBER)
	{
		*pValidCalculation = BAD_CALCULATION;
	}
	else
	{
		*pValidCalculation = GOOD_CALCULATION;
	}
	return exponent;
}

int main(void) {
	bool validCalculation = GOOD_CALCULATION;
	int result = 0;
	fPtr calc_functions[3] = { add, multiply, pow };
	std::string func_names[3] = { "add", "multiply", "pow" };

	for (int i = 0; i < 3; i++)
	{
		result = calc_functions[i](82, 1, &validCalculation); //First add, second multiply, third pow.
		if (validCalculation)
		{
			std::cout << "The " << func_names[i] << " result is: " << result << std::endl;
		}
		else
		{
			std::cout << DENIDE_MESSAGE << std::endl;
		}
	}

	getchar();
	return 0;
}