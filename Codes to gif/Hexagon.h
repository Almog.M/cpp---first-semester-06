#pragma once
#include "shape.h"
#include <string>
using std::string;

class Hexagon :
	public Shape
{
public:
	Hexagon(string name, string color, double rib);
	virtual ~Hexagon();
	virtual void draw(); //DEFINE FOR ALL
	virtual double CalArea();//DEFINE FOR ALL
	void setRib(double rib);
	double getRib() const;
private:
	double _rib;
};

