#include <iostream>
#include <Math.h>
#include <string>
//iF WE AHAVE AN EXCEPTION WE THROW THE FORBIDDEN NUMBER - 8200.


#define EXCEPTION "This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year."
#define FORBIDDEN_NUMBER 8200

typedef int(*fPtr) (int, int);

int add(int a, int b) {
	if (a == FORBIDDEN_NUMBER || b == FORBIDDEN_NUMBER || a+b == FORBIDDEN_NUMBER)
	{
		throw a + b;
	}
	
  return a + b;
}

int  multiply(int a, int b) {
  int sum = 0;
  for(int i = 0; i < b; i++) {
	  try
	  {
		  sum = add(sum, a);
	  }
	  catch (int exception)
	  {
		  sum = exception;
	  }
  };
  if (a == FORBIDDEN_NUMBER || b == FORBIDDEN_NUMBER || sum == FORBIDDEN_NUMBER)
  {
	  throw sum;
  }
  return sum;
}

int  pow(int a, int b) {
  int exponent = 1;
  for(int i = 0; i < b; i++) {
	  try
	  {
		  exponent = multiply(exponent, a);
	  }
	  catch (int exception)
	  {
		  exponent = exception;
	  }
  };
  if (a == FORBIDDEN_NUMBER || b == FORBIDDEN_NUMBER || exponent == FORBIDDEN_NUMBER)
  {
	  throw exponent;
  }
  return exponent;
}

int main(void) {
	fPtr calc_functions[3] = { add, multiply, pow };
	std::string func_names[3] = { "add", "multiply", "pow" };
	for (int i = 0; i < 3; i++)
	{
		try
		{
			std::cout << "The " <<  func_names[i] << " result is: " << calc_functions[i](8200, 1) << std::endl; //First add, second multiply, third pow.
		}
		catch (...)
		{
			std::cout << EXCEPTION << std::endl;
		}
	}

	getchar();
}