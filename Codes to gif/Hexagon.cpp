#include "Hexagon.h"
#include "MathUtils.h"
#include "ShapeException.h" 

Hexagon::Hexagon(string name, string color, double rib) : Shape(name, color)
{
	setRib(rib);
}

Hexagon::~Hexagon()
{

}

void Hexagon::draw()
{
	std::cout << std::endl << "Color is " << getColor() << std::endl << "Name is " << getName() << std::endl << "area is " << CalArea() << std::endl << std::endl;
}

double Hexagon::CalArea()
{
	return MathUtils::CalHexagonArea(_rib);
}

void Hexagon::setRib(double rib)
{
	_rib = rib;
	if (_rib < 0)
		throw shapeException();
}

double Hexagon::getRib() const
{
	return _rib;
}
