#pragma once
#include "shape.h"
#include "MathUtils.h"
#include <string>
using std::string;

class Pentagon :
	public Shape
{
public:
	Pentagon(string name, string color, double rib);
	virtual void draw(); //DEFINE FOR ALL
	virtual double CalArea();//DEFINE FOR ALL
	virtual ~Pentagon();
	void setRib(double rib);
	double getRib() const;
private:
	double _rib;
};

