#include "MathUtils.h"
#include <Math.h>



double MathUtils::CalPentagonArea(double rib)
{
	return 0.25 * sqrt(5*(5+2*sqrt(5))) * pow(rib, 2);
}

double MathUtils::CalHexagonArea(double rib)
{
	return ((3*sqrt(3)) / 2) * pow(rib, 2);
}

MathUtils::MathUtils()
{
}


MathUtils::~MathUtils()
{
}
