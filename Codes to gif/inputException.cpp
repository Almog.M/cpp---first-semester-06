#include "inputException.h"

inputException::inputException()
{
	_exception = "Non";
}

inputException::inputException(const char* exception)
{
	_exception = exception;
}

inputException::~inputException()
{
	_exception = nullptr;
}


const char * inputException::what() const
{
	return _exception;
}

void inputException::checkInputValidation(const char* possibleException) const
{
	if (getchar() != ENTER_CHAR)
		throw inputException(possibleException);
}

void buffer::clearBuffer()
{
	char ch = getchar();
	while (ch != ENTER_CHAR)
	{
		ch = getchar();
	}
}
